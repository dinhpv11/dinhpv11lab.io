$(document).ready(function() {
	$('.close-menu a i').click(function(event) {
		$('.close-menu').toggleClass('open-event');
		$('#side-menu').toggleClass('open-event-menu');
		$('#wrapper-content').toggleClass('open-content');
		return false;
	});

	var options = {
		useEasing: 'true',
		useGrouping: true, 
		separator: ',', 
		decimal: '.', 
	};
	var demo1 = new CountUp('number-count-1', 0, 777, 0, 2.5, options);
	var demo2 = new CountUp('number-count-2', 0, 333, 0, 2.5, options);
	var demo3 = new CountUp('number-count-3', 0, 123, 0, 2.5, options);
	var demo4 = new CountUp('number-count-4', 0, 300, 0, 2.5, options);



	var typed1 = new Typed("#typed-1", {
	stringsElement: '#typed-strings-1',
	typeSpeed: 30,
	backSpeed: 30,
	backDelay: 1000,
	startDelay: 1000,
	loop: true,
	});

	var typed2 = new Typed("#typed-2", {
	stringsElement: '#typed-strings-2',
	typeSpeed: 30,
	backSpeed: 30,
	backDelay: 1000,
	startDelay: 1000,
	loop: true,
	});

	$('.owl-1').owlCarousel({
		loop:true,
		margin:10,
		autoplay:3000,
		responsive:{
			0:{
				items:1
			},
			768:{
				items:3
			},
		}
	})

	$('.nenden').click(function(){
		$('#side-menu').removeClass('open-event-menu');
		$('#wrapper-content').removeClass('open-content');
	})

	$(window).scroll(function(event){
		var vitrihientai = $('html').scrollTop();
		console.log(vitrihientai);

		if(vitrihientai > ($('#skill-content').offset().top - 400)){
			$('.skill-progress').addClass('active-skill');
		}

		// Count Up

		// if (vitrihientai > ($('#countup').offset().top) - 600) {
		// 		if (!demo1.error) {
		// 		  demo1.start();
		// 		} else {
		// 		  console.error(demo1.error);
		// 		}
		// 		if (!demo2.error) {
		// 		  demo2.start();
		// 		} else {
		// 		  console.error(demo2.error);
		// 		}
		// 		if (!demo3.error) {
		// 		  demo3.start();
		// 		} else {
		// 		  console.error(demo3.error);
		// 		}
		// 		if (!demo4.error) {
		// 		  demo4.start();
		// 		} else {
		// 		  console.error(demo4.error);
		// 		}
		// }

		// End Count Up
	})
});